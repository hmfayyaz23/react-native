import React, { Component } from 'react';
import { View } from 'react-native';
import AppBar from './app/components/AppBar';
import MySurahs from './app/components/MySurahs';
export default class App extends Component {
    render() {
        return (
            <View style={{ flex: 1 }}>
                <AppBar />
                <MySurahs />
            </View>
        );
    }
}