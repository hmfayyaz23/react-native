import React, { Component } from 'react';
import {  DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import Main from './Main';

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: '#2196F3',
    secondary: '#ffffff',
    accent: '#f1c40f',
  }
};

export default class App extends Component {
  render() {
    return (
      <PaperProvider theme={theme}>
        <Main/>
      </PaperProvider>
    );
  }
}