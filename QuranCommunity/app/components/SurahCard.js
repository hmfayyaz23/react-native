import React from 'react';
import { TouchableOpacity, Text, View, ImageBackground } from 'react-native';
import { Avatar, Button, Card, Title, Paragraph } from 'react-native-paper';
//import styles for your SurahCard component.
import styles from './styles';
//Define your stateless componetns, and destrcuts props from function arguments
const SurahCard = ({ surah }) => {
    return (
        <TouchableOpacity onPress={() => console.log('Pressed')}>
            <View style={styles.listItemContainer}>

                <View style={styles.surahNumberContainer}>
                    <ImageBackground source={require('../../assets/surahnumber.png')} style={styles.surahNumberImage}>
                        <Text style={styles.surahNumber}>{surah.id}</Text>
                    </ImageBackground>
                </View>

                <View style={styles.translationContainer}>
                    <Text style={styles.nameInEnglish}>{surah.nameInEnglish}</Text>
                    <Text style={styles.englishTransliteration}>{surah.englishTransliteration}</Text>
                </View>

                <View style={styles.arabicContainer}>
                    <Text style={styles.nameInArabic}>{surah.nameInArabic}</Text>
                </View>

            </View>
        </TouchableOpacity>
    );
}
export default SurahCard;