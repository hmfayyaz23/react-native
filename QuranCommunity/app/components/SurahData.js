export const Surahs = [
    {
        "id": 1,
        "englishTransliteration": "The Opening",
        "nameInEnglish": "Al Fatihah",
        "nameInArabic": "سورة الفاتحة",
        "number": 1,
        "startAyahId": 1,
        "endAyahId": 7,
        "origin": "Mecca"
      },
      {
        "id": 2,
        "englishTransliteration": "The Cow",
        "nameInEnglish": "Al Baqarah",
        "nameInArabic": "سورة البقرة",
        "number": 2,
        "startAyahId": 8,
        "endAyahId": 293,
        "origin": "Medina"
      },
      {
        "id": 3,
        "englishTransliteration": "The Family of Imran",
        "nameInEnglish": "Al 'Imran",
        "nameInArabic": "سورة آل عمران",
        "number": 3,
        "startAyahId": 294,
        "endAyahId": 493,
        "origin": "Medina"
      },
      {
        "id": 4,
        "englishTransliteration": "The Women",
        "nameInEnglish": "An Nisa",
        "nameInArabic": "سورة النساء",
        "number": 4,
        "startAyahId": 494,
        "endAyahId": 669,
        "origin": "Medina"
      },
      {
        "id": 5,
        "englishTransliteration": "The Table Spread with Food",
        "nameInEnglish": "Al Maidah",
        "nameInArabic": "سورة المائدة",
        "number": 5,
        "startAyahId": 670,
        "endAyahId": 789,
        "origin": "Medina"
      },
      {
        "id": 6,
        "englishTransliteration": "The Cattles",
        "nameInEnglish": "Al An'am",
        "nameInArabic": "سورة الأنعام",
        "number": 6,
        "startAyahId": 790,
        "endAyahId": 954,
        "origin": "Mecca"
      },
      {
        "id": 7,
        "englishTransliteration": "The Heights",
        "nameInEnglish": "Al A'raf",
        "nameInArabic": "سورة الأعراف",
        "number": 7,
        "startAyahId": 955,
        "endAyahId": 1160,
        "origin": "Mecca"
      },
      {
        "id": 8,
        "englishTransliteration": "The Spoils of War",
        "nameInEnglish": "Al Anfal",
        "nameInArabic": "سورة الأنفال",
        "number": 8,
        "startAyahId": 1161,
        "endAyahId": 1235,
        "origin": "Medina"
      },
      {
        "id": 9,
        "englishTransliteration": "The Repentance",
        "nameInEnglish": "At Tawbah",
        "nameInArabic": "سورة التوبة",
        "number": 9,
        "startAyahId": 1236,
        "endAyahId": 1364,
        "origin": "Medina"
      },
      {
        "id": 10,
        "englishTransliteration": "Jonah",
        "nameInEnglish": "Yunus",
        "nameInArabic": "سورة يونس",
        "number": 10,
        "startAyahId": 1365,
        "endAyahId": 1473,
        "origin": "Mecca"
      },
      {
        "id": 11,
        "englishTransliteration": "Hud",
        "nameInEnglish": "Hud",
        "nameInArabic": "سورة هود",
        "number": 11,
        "startAyahId": 1474,
        "endAyahId": 1596,
        "origin": "Mecca"
      },
      {
        "id": 12,
        "englishTransliteration": "Joseph",
        "nameInEnglish": "Yusuf",
        "nameInArabic": "سورة يوسف",
        "number": 12,
        "startAyahId": 1597,
        "endAyahId": 1707,
        "origin": "Mecca"
      },
      {
        "id": 13,
        "englishTransliteration": "The Thunder",
        "nameInEnglish": "Ar Ra'd",
        "nameInArabic": "سورة الرّعد",
        "number": 13,
        "startAyahId": 1708,
        "endAyahId": 1750,
        "origin": "Medina"
      },
      {
        "id": 14,
        "englishTransliteration": "Abraham",
        "nameInEnglish": "Ibrahim",
        "nameInArabic": "سورة إبراهيم",
        "number": 14,
        "startAyahId": 1751,
        "endAyahId": 1802,
        "origin": "Mecca"
      },
      {
        "id": 15,
        "englishTransliteration": "The Rocky Tract",
        "nameInEnglish": "Al Hijr",
        "nameInArabic": "سورة الحجر",
        "number": 15,
        "startAyahId": 1803,
        "endAyahId": 1901,
        "origin": "Mecca"
      },
      {
        "id": 16,
        "englishTransliteration": "The Bees",
        "nameInEnglish": "An Nahl",
        "nameInArabic": "سورة النحل",
        "number": 16,
        "startAyahId": 1902,
        "endAyahId": 2029,
        "origin": "Mecca"
      },
      {
        "id": 17,
        "englishTransliteration": "The Journey by Night",
        "nameInEnglish": "Al Isra",
        "nameInArabic": "سورة الإسراء",
        "number": 17,
        "startAyahId": 2030,
        "endAyahId": 2140,
        "origin": "Mecca"
      },
      {
        "id": 18,
        "englishTransliteration": "The Cave",
        "nameInEnglish": "Al Kahf",
        "nameInArabic": "سورة الكهف",
        "number": 18,
        "startAyahId": 2141,
        "endAyahId": 2250,
        "origin": "Mecca"
      },
      {
        "id": 19,
        "englishTransliteration": "Mary",
        "nameInEnglish": "Maryam",
        "nameInArabic": "سورة مريم",
        "number": 19,
        "startAyahId": 2251,
        "endAyahId": 2348,
        "origin": "Mecca"
      },
      {
        "id": 20,
        "englishTransliteration": "Ta Ha",
        "nameInEnglish": "Ta Ha",
        "nameInArabic": "سورة طه",
        "number": 20,
        "startAyahId": 2349,
        "endAyahId": 2483,
        "origin": "Mecca"
      },
      {
        "id": 21,
        "englishTransliteration": "The Prophets",
        "nameInEnglish": "Al Anbiya",
        "nameInArabic": "سورة الأنبياء",
        "number": 21,
        "startAyahId": 2484,
        "endAyahId": 2595,
        "origin": "Mecca"
      },
      {
        "id": 22,
        "englishTransliteration": "The Pilgrimage",
        "nameInEnglish": "Al Hajj",
        "nameInArabic": "سورة الحج",
        "number": 22,
        "startAyahId": 2596,
        "endAyahId": 2673,
        "origin": "Medina"
      },
      {
        "id": 23,
        "englishTransliteration": "The Believers",
        "nameInEnglish": "Al Muminun",
        "nameInArabic": "سورة المؤمنون",
        "number": 23,
        "startAyahId": 2674,
        "endAyahId": 2791,
        "origin": "Mecca"
      },
      {
        "id": 24,
        "englishTransliteration": "The Light",
        "nameInEnglish": "An Nur",
        "nameInArabic": "سورة النّور",
        "number": 24,
        "startAyahId": 2792,
        "endAyahId": 2855,
        "origin": "Medina"
      },
      {
        "id": 25,
        "englishTransliteration": "The Criterion",
        "nameInEnglish": "Al Furqan",
        "nameInArabic": "سورة الفرقان",
        "number": 25,
        "startAyahId": 2856,
        "endAyahId": 2932,
        "origin": "Mecca"
      },
      {
        "id": 26,
        "englishTransliteration": "The Poets",
        "nameInEnglish": "Ash Shu'ara",
        "nameInArabic": "سورة الشعراء",
        "number": 26,
        "startAyahId": 2933,
        "endAyahId": 3159,
        "origin": "Mecca"
      },
      {
        "id": 27,
        "englishTransliteration": "The Ants",
        "nameInEnglish": "An Naml",
        "nameInArabic": "سورة النمل",
        "number": 27,
        "startAyahId": 3160,
        "endAyahId": 3252,
        "origin": "Mecca"
      },
      {
        "id": 28,
        "englishTransliteration": "The Narration",
        "nameInEnglish": "Al Qasas",
        "nameInArabic": "سورة القصص",
        "number": 28,
        "startAyahId": 3253,
        "endAyahId": 3340,
        "origin": "Mecca"
      },
      {
        "id": 29,
        "englishTransliteration": "The Spider",
        "nameInEnglish": "Al 'Ankabut",
        "nameInArabic": "سورة العنكبوت",
        "number": 29,
        "startAyahId": 3341,
        "endAyahId": 3409,
        "origin": "Mecca"
      },
      {
        "id": 30,
        "englishTransliteration": "Rome",
        "nameInEnglish": "Ar Rum",
        "nameInArabic": "سورة الروم",
        "number": 30,
        "startAyahId": 3410,
        "endAyahId": 3469,
        "origin": "Mecca"
      },
      {
        "id": 31,
        "englishTransliteration": "Luqman",
        "nameInEnglish": "Luqman",
        "nameInArabic": "سورة لقمان",
        "number": 31,
        "startAyahId": 3470,
        "endAyahId": 3503,
        "origin": "Mecca"
      },
      {
        "id": 32,
        "englishTransliteration": "The Prostration",
        "nameInEnglish": "As Sajdah",
        "nameInArabic": "سورة السجدة",
        "number": 32,
        "startAyahId": 3504,
        "endAyahId": 3533,
        "origin": "Mecca"
      },
      {
        "id": 33,
        "englishTransliteration": "The Confederates",
        "nameInEnglish": "Al Ahzab",
        "nameInArabic": "سورة الأحزاب",
        "number": 33,
        "startAyahId": 3534,
        "endAyahId": 3606,
        "origin": "Medina"
      },
      {
        "id": 34,
        "englishTransliteration": "Sheba",
        "nameInEnglish": "Saba",
        "nameInArabic": "سورة سبإ",
        "number": 34,
        "startAyahId": 3607,
        "endAyahId": 3660,
        "origin": "Mecca"
      },
      {
        "id": 35,
        "englishTransliteration": "The Originator of Creation",
        "nameInEnglish": "Fatir",
        "nameInArabic": "سورة فاطر",
        "number": 35,
        "startAyahId": 3661,
        "endAyahId": 3705,
        "origin": "Mecca"
      },
      {
        "id": 36,
        "englishTransliteration": "Ya Sin",
        "nameInEnglish": "Ya Sin",
        "nameInArabic": "سورة يس",
        "number": 36,
        "startAyahId": 3706,
        "endAyahId": 3788,
        "origin": "Mecca"
      },
      {
        "id": 37,
        "englishTransliteration": "Those Ranged in Ranks",
        "nameInEnglish": "As Saffat",
        "nameInArabic": "سورة الصّافّات",
        "number": 37,
        "startAyahId": 3789,
        "endAyahId": 3970,
        "origin": "Mecca"
      },
      {
        "id": 38,
        "englishTransliteration": "Sad",
        "nameInEnglish": "Sad",
        "nameInArabic": "سورة ص",
        "number": 38,
        "startAyahId": 3971,
        "endAyahId": 4058,
        "origin": "Mecca"
      },
      {
        "id": 39,
        "englishTransliteration": "The Groups",
        "nameInEnglish": "Az Zumar",
        "nameInArabic": "سورة الزمر",
        "number": 39,
        "startAyahId": 4059,
        "endAyahId": 4133,
        "origin": "Mecca"
      },
      {
        "id": 40,
        "englishTransliteration": "The Forgiver",
        "nameInEnglish": "Ghafir",
        "nameInArabic": "سورة غافر",
        "number": 40,
        "startAyahId": 4134,
        "endAyahId": 4218,
        "origin": "Mecca"
      },
      {
        "id": 41,
        "englishTransliteration": "They are Explained in Detail",
        "nameInEnglish": "Fussilat",
        "nameInArabic": "سورة فصّلت",
        "number": 41,
        "startAyahId": 4219,
        "endAyahId": 4272,
        "origin": "Mecca"
      },
      {
        "id": 42,
        "englishTransliteration": "The Consultations",
        "nameInEnglish": "Ash-Shura",
        "nameInArabic": "سورة الشورى",
        "number": 42,
        "startAyahId": 4273,
        "endAyahId": 4325,
        "origin": "Mecca"
      },
      {
        "id": 43,
        "englishTransliteration": "The Gold Adornments",
        "nameInEnglish": "Az Zukhruf",
        "nameInArabic": "سورة الزخرف",
        "number": 43,
        "startAyahId": 4326,
        "endAyahId": 4414,
        "origin": "Mecca"
      },
      {
        "id": 44,
        "englishTransliteration": "The Smoke",
        "nameInEnglish": "Ad Dukhan",
        "nameInArabic": "سورة الدخان",
        "number": 44,
        "startAyahId": 4415,
        "endAyahId": 4473,
        "origin": "Mecca"
      },
      {
        "id": 45,
        "englishTransliteration": "The Kneeling",
        "nameInEnglish": "Al Jathiyah",
        "nameInArabic": "سورة الجاثية",
        "number": 45,
        "startAyahId": 4474,
        "endAyahId": 4510,
        "origin": "Mecca"
      },
      {
        "id": 46,
        "englishTransliteration": "The Curved Sand-Hills",
        "nameInEnglish": "Al Ahqaf",
        "nameInArabic": "سورة الأحقاف",
        "number": 46,
        "startAyahId": 4511,
        "endAyahId": 4545,
        "origin": "Mecca"
      },
      {
        "id": 47,
        "englishTransliteration": "Muhammad S",
        "nameInEnglish": "Muhammad",
        "nameInArabic": "سورة محمّـد",
        "number": 47,
        "startAyahId": 4546,
        "endAyahId": 4583,
        "origin": "Medina"
      },
      {
        "id": 48,
        "englishTransliteration": "The Victory",
        "nameInEnglish": "Al Fath",
        "nameInArabic": "سورة الفتح",
        "number": 48,
        "startAyahId": 4584,
        "endAyahId": 4612,
        "origin": "Medina"
      },
      {
        "id": 49,
        "englishTransliteration": "The Dwellings",
        "nameInEnglish": "Al Hujurat",
        "nameInArabic": "سورة الحُـجُـرات",
        "number": 49,
        "startAyahId": 4613,
        "endAyahId": 4630,
        "origin": "Medina"
      },
      {
        "id": 50,
        "englishTransliteration": "Qaf",
        "nameInEnglish": "Qaf",
        "nameInArabic": "سورة ق",
        "number": 50,
        "startAyahId": 4631,
        "endAyahId": 4675,
        "origin": "Mecca"
      },
      {
        "id": 51,
        "englishTransliteration": "The Winds that Scatter",
        "nameInEnglish": "Adh Dhariyat",
        "nameInArabic": "سورة الذاريات",
        "number": 51,
        "startAyahId": 4676,
        "endAyahId": 4735,
        "origin": "Mecca"
      },
      {
        "id": 52,
        "englishTransliteration": "The Mount",
        "nameInEnglish": "At Tur",
        "nameInArabic": "سورة الـطور",
        "number": 52,
        "startAyahId": 4736,
        "endAyahId": 4784,
        "origin": "Mecca"
      },
      {
        "id": 53,
        "englishTransliteration": "The Star",
        "nameInEnglish": "An Najm",
        "nameInArabic": "سورة الـنحـم",
        "number": 53,
        "startAyahId": 4785,
        "endAyahId": 4846,
        "origin": "Mecca"
      },
      {
        "id": 54,
        "englishTransliteration": "The Moon",
        "nameInEnglish": "Al Qamar",
        "nameInArabic": "سورة الـقمـر",
        "number": 54,
        "startAyahId": 4847,
        "endAyahId": 4901,
        "origin": "Mecca"
      },
      {
        "id": 55,
        "englishTransliteration": "The Most Gracious",
        "nameInEnglish": "Ar Rahman",
        "nameInArabic": "سورة الـرحـمـن",
        "number": 55,
        "startAyahId": 4902,
        "endAyahId": 4979,
        "origin": "Medina"
      },
      {
        "id": 56,
        "englishTransliteration": "The Event",
        "nameInEnglish": "Al Waqi'ah",
        "nameInArabic": "سورة الواقيـة",
        "number": 56,
        "startAyahId": 4980,
        "endAyahId": 5075,
        "origin": "Mecca"
      },
      {
        "id": 57,
        "englishTransliteration": "Iron",
        "nameInEnglish": "Al Hadid",
        "nameInArabic": "سورة الحـديد",
        "number": 57,
        "startAyahId": 5076,
        "endAyahId": 5104,
        "origin": "Medina"
      },
      {
        "id": 58,
        "englishTransliteration": "The Woman Who Disputes",
        "nameInEnglish": "Al Mujadilah",
        "nameInArabic": "سورة الـمجادلـة",
        "number": 58,
        "startAyahId": 5105,
        "endAyahId": 5126,
        "origin": "Medina"
      },
      {
        "id": 59,
        "englishTransliteration": "The Gathering",
        "nameInEnglish": "Al Hashr",
        "nameInArabic": "سورة الـحـشـر",
        "number": 59,
        "startAyahId": 5127,
        "endAyahId": 5150,
        "origin": "Medina"
      },
      {
        "id": 60,
        "englishTransliteration": "The Woman to be examined",
        "nameInEnglish": "Al Mumtahanah",
        "nameInArabic": "سورة الـمـمـتـحنة",
        "number": 60,
        "startAyahId": 5151,
        "endAyahId": 5163,
        "origin": "Medina"
      },
      {
        "id": 61,
        "englishTransliteration": "The Row or Rank",
        "nameInEnglish": "As Saff",
        "nameInArabic": "سورة الـصّـف",
        "number": 61,
        "startAyahId": 5164,
        "endAyahId": 5177,
        "origin": "Medina"
      },
      {
        "id": 62,
        "englishTransliteration": "Friday",
        "nameInEnglish": "Al Jumu'ah",
        "nameInArabic": "سورة الـجـمـعـة",
        "number": 62,
        "startAyahId": 5178,
        "endAyahId": 5188,
        "origin": "Medina"
      },
      {
        "id": 63,
        "englishTransliteration": "The Hypocrites",
        "nameInEnglish": "Al Munafiqun",
        "nameInArabic": "سورة الـمنافقون",
        "number": 63,
        "startAyahId": 5189,
        "endAyahId": 5199,
        "origin": "Medina"
      },
      {
        "id": 64,
        "englishTransliteration": "Mutual Loss and Gain",
        "nameInEnglish": "At Taghabun",
        "nameInArabic": "سورة الـتغابن",
        "number": 64,
        "startAyahId": 5200,
        "endAyahId": 5217,
        "origin": "Medina"
      },
      {
        "id": 65,
        "englishTransliteration": "The Divorce",
        "nameInEnglish": "At Talaq",
        "nameInArabic": "سورة الـطلاق",
        "number": 65,
        "startAyahId": 5218,
        "endAyahId": 5229,
        "origin": "Medina"
      },
      {
        "id": 66,
        "englishTransliteration": "The Prohibition",
        "nameInEnglish": "At Tahrim",
        "nameInArabic": "سورة الـتحريم",
        "number": 66,
        "startAyahId": 5230,
        "endAyahId": 5241,
        "origin": "Medina"
      },
      {
        "id": 67,
        "englishTransliteration": "Dominion",
        "nameInEnglish": "Al Mulk",
        "nameInArabic": "سورة الـملك",
        "number": 67,
        "startAyahId": 5242,
        "endAyahId": 5271,
        "origin": "Mecca"
      },
      {
        "id": 68,
        "englishTransliteration": "The Pen",
        "nameInEnglish": "Al Qalam",
        "nameInArabic": "سورة الـقـلـم",
        "number": 68,
        "startAyahId": 5272,
        "endAyahId": 5323,
        "origin": "Mecca"
      },
      {
        "id": 69,
        "englishTransliteration": "The Inevitable",
        "nameInEnglish": "Al Haqqah",
        "nameInArabic": "سورة الـحاقّـة",
        "number": 69,
        "startAyahId": 5324,
        "endAyahId": 5375,
        "origin": "Mecca"
      },
      {
        "id": 70,
        "englishTransliteration": "The Ways of Ascent",
        "nameInEnglish": "Al Ma'arij",
        "nameInArabic": "سورة الـمعارج",
        "number": 70,
        "startAyahId": 5376,
        "endAyahId": 5419,
        "origin": "Mecca"
      },
      {
        "id": 71,
        "englishTransliteration": "Noah",
        "nameInEnglish": "Nuh",
        "nameInArabic": "سورة نوح",
        "number": 71,
        "startAyahId": 5420,
        "endAyahId": 5447,
        "origin": "Mecca"
      },
      {
        "id": 72,
        "englishTransliteration": "The Jinn",
        "nameInEnglish": "Al Jinn",
        "nameInArabic": "سورة الجن",
        "number": 72,
        "startAyahId": 5448,
        "endAyahId": 5475,
        "origin": "Mecca"
      },
      {
        "id": 73,
        "englishTransliteration": "The One Wraped in Garments",
        "nameInEnglish": "Al Muzzammil",
        "nameInArabic": "سورة الـمـزّمّـل",
        "number": 73,
        "startAyahId": 5476,
        "endAyahId": 5495,
        "origin": "Mecca"
      },
      {
        "id": 74,
        "englishTransliteration": "The One Enveloped",
        "nameInEnglish": "Al Muddaththir",
        "nameInArabic": "سورة الـمّـدّثّـر",
        "number": 74,
        "startAyahId": 5496,
        "endAyahId": 5551,
        "origin": "Mecca"
      },
      {
        "id": 75,
        "englishTransliteration": "The Resurrection",
        "nameInEnglish": "Al Qiyamah",
        "nameInArabic": "سورة الـقـيامـة",
        "number": 75,
        "startAyahId": 5552,
        "endAyahId": 5591,
        "origin": "Mecca"
      },
      {
        "id": 76,
        "englishTransliteration": "Man or Time",
        "nameInEnglish": "Al Insan or Ad Dahar",
        "nameInArabic": "سورة الإنسان",
        "number": 76,
        "startAyahId": 5592,
        "endAyahId": 5622,
        "origin": "Medina"
      },
      {
        "id": 77,
        "englishTransliteration": "Those sent forth",
        "nameInEnglish": "Al Mursalat",
        "nameInArabic": "سورة الـمرسلات",
        "number": 77,
        "startAyahId": 5623,
        "endAyahId": 5672,
        "origin": "Mecca"
      },
      {
        "id": 78,
        "englishTransliteration": "The Great News",
        "nameInEnglish": "An Naba",
        "nameInArabic": "سورة الـنبإ",
        "number": 78,
        "startAyahId": 5673,
        "endAyahId": 5712,
        "origin": "Mecca"
      },
      {
        "id": 79,
        "englishTransliteration": "Those Who Pull Out",
        "nameInEnglish": "An Nazi'at",
        "nameInArabic": "سورة الـنازعات",
        "number": 79,
        "startAyahId": 5713,
        "endAyahId": 5758,
        "origin": "Mecca"
      },
      {
        "id": 80,
        "englishTransliteration": "He Frowned",
        "nameInEnglish": "Abasa",
        "nameInArabic": "سورة عبس",
        "number": 80,
        "startAyahId": 5759,
        "endAyahId": 5800,
        "origin": "Mecca"
      },
      {
        "id": 81,
        "englishTransliteration": "Wound Round and Lost its Light",
        "nameInEnglish": "At-Takwir",
        "nameInArabic": "سورة التكوير",
        "number": 81,
        "startAyahId": 5801,
        "endAyahId": 5829,
        "origin": "Mecca"
      },
      {
        "id": 82,
        "englishTransliteration": "The Cleaving",
        "nameInEnglish": "Al Infitar",
        "nameInArabic": "سورة الانفطار",
        "number": 82,
        "startAyahId": 5830,
        "endAyahId": 5848,
        "origin": "Mecca"
      },
      {
        "id": 83,
        "englishTransliteration": "Those Who Deal in Fraud",
        "nameInEnglish": "Al Mutaffifin",
        "nameInArabic": "سورة المطـفـفين",
        "number": 83,
        "startAyahId": 5849,
        "endAyahId": 5884,
        "origin": "Mecca"
      },
      {
        "id": 84,
        "englishTransliteration": "The Splitting Asunder",
        "nameInEnglish": "Al Inshiqaq",
        "nameInArabic": "سورة الانشقاق",
        "number": 84,
        "startAyahId": 5885,
        "endAyahId": 5909,
        "origin": "Mecca"
      },
      {
        "id": 85,
        "englishTransliteration": "The Big Stars",
        "nameInEnglish": "Al Buruj",
        "nameInArabic": "سورة البروج",
        "number": 85,
        "startAyahId": 5910,
        "endAyahId": 5931,
        "origin": "Mecca"
      },
      {
        "id": 86,
        "englishTransliteration": "The Night Commer",
        "nameInEnglish": "At Tariq",
        "nameInArabic": "سورة الـطارق",
        "number": 86,
        "startAyahId": 5932,
        "endAyahId": 5948,
        "origin": "Mecca"
      },
      {
        "id": 87,
        "englishTransliteration": "The Most High",
        "nameInEnglish": "Al A'la",
        "nameInArabic": "سورة الأعـلى",
        "number": 87,
        "startAyahId": 5949,
        "endAyahId": 5967,
        "origin": "Mecca"
      },
      {
        "id": 88,
        "englishTransliteration": "The Overwhelming",
        "nameInEnglish": "Al Ghashiyah",
        "nameInArabic": "سورة الغاشـيـة",
        "number": 88,
        "startAyahId": 5968,
        "endAyahId": 5993,
        "origin": "Mecca"
      },
      {
        "id": 89,
        "englishTransliteration": "The Break of Day or The Dawn",
        "nameInEnglish": "Al Fajr",
        "nameInArabic": "سورة الفجر",
        "number": 89,
        "startAyahId": 5994,
        "endAyahId": 6023,
        "origin": "Mecca"
      },
      {
        "id": 90,
        "englishTransliteration": "The City",
        "nameInEnglish": "Al Balad",
        "nameInArabic": "سورة الـبلد",
        "number": 90,
        "startAyahId": 6024,
        "endAyahId": 6043,
        "origin": "Mecca"
      },
      {
        "id": 91,
        "englishTransliteration": "The Sun",
        "nameInEnglish": "Ash-Shams",
        "nameInArabic": "سورة الـشـمـس",
        "number": 91,
        "startAyahId": 6044,
        "endAyahId": 6058,
        "origin": "Mecca"
      },
      {
        "id": 92,
        "englishTransliteration": "The Night",
        "nameInEnglish": "Al Layl",
        "nameInArabic": "سورة اللـيـل",
        "number": 92,
        "startAyahId": 6059,
        "endAyahId": 6079,
        "origin": "Mecca"
      },
      {
        "id": 93,
        "englishTransliteration": "The Forenoon",
        "nameInEnglish": "Ad Duha",
        "nameInArabic": "سورة الضـحى",
        "number": 93,
        "startAyahId": 6080,
        "endAyahId": 6090,
        "origin": "Mecca"
      },
      {
        "id": 94,
        "englishTransliteration": "The Opening Forth",
        "nameInEnglish": "Ash Sharh",
        "nameInArabic": "سورة الـشرح",
        "number": 94,
        "startAyahId": 6091,
        "endAyahId": 6098,
        "origin": "Mecca"
      },
      {
        "id": 95,
        "englishTransliteration": "The Fig",
        "nameInEnglish": "At Tin",
        "nameInArabic": "سورة الـتين",
        "number": 95,
        "startAyahId": 6099,
        "endAyahId": 6106,
        "origin": "Mecca"
      },
      {
        "id": 96,
        "englishTransliteration": "The Clot",
        "nameInEnglish": "Al 'Alaq",
        "nameInArabic": "سورة الـعلق",
        "number": 96,
        "startAyahId": 6107,
        "endAyahId": 6125,
        "origin": "Mecca"
      },
      {
        "id": 97,
        "englishTransliteration": "The Night of Decree",
        "nameInEnglish": "Al Qadr",
        "nameInArabic": "سورة الـقدر",
        "number": 97,
        "startAyahId": 6126,
        "endAyahId": 6130,
        "origin": "Mecca"
      },
      {
        "id": 98,
        "englishTransliteration": "The Clear Evidence",
        "nameInEnglish": "Al Bayyinah",
        "nameInArabic": "سورة الـبينة",
        "number": 98,
        "startAyahId": 6131,
        "endAyahId": 6138,
        "origin": "Medina"
      },
      {
        "id": 99,
        "englishTransliteration": "The Earthquake",
        "nameInEnglish": "Az-Zalzalah",
        "nameInArabic": "سورة الـزلزلة",
        "number": 99,
        "startAyahId": 6139,
        "endAyahId": 6146,
        "origin": "Medina"
      },
      {
        "id": 100,
        "englishTransliteration": "Those That Run",
        "nameInEnglish": "Al 'Adiyat",
        "nameInArabic": "سورة الـعاديات",
        "number": 100,
        "startAyahId": 6147,
        "endAyahId": 6157,
        "origin": "Mecca"
      },
      {
        "id": 101,
        "englishTransliteration": "The Striking Hour",
        "nameInEnglish": "Al Qari'ah",
        "nameInArabic": "سورة الـقارعـة",
        "number": 101,
        "startAyahId": 6158,
        "endAyahId": 6168,
        "origin": "Mecca"
      },
      {
        "id": 102,
        "englishTransliteration": "The Piling Up",
        "nameInEnglish": "At Takathur",
        "nameInArabic": "سورة الـتكاثر",
        "number": 102,
        "startAyahId": 6169,
        "endAyahId": 6176,
        "origin": "Mecca"
      },
      {
        "id": 103,
        "englishTransliteration": "The Time",
        "nameInEnglish": "Al 'Asr",
        "nameInArabic": "سورة الـعصر",
        "number": 103,
        "startAyahId": 6177,
        "endAyahId": 6179,
        "origin": "Mecca"
      },
      {
        "id": 104,
        "englishTransliteration": "The Slanderer",
        "nameInEnglish": "Al Humazah",
        "nameInArabic": "سورة الـهمزة",
        "number": 104,
        "startAyahId": 6180,
        "endAyahId": 6188,
        "origin": "Mecca"
      },
      {
        "id": 105,
        "englishTransliteration": "The Elephant",
        "nameInEnglish": "Al Fil",
        "nameInArabic": "سورة الـفيل",
        "number": 105,
        "startAyahId": 6189,
        "endAyahId": 6193,
        "origin": "Mecca"
      },
      {
        "id": 106,
        "englishTransliteration": "Quraysh",
        "nameInEnglish": "Quraysh",
        "nameInArabic": "سورة قريش",
        "number": 106,
        "startAyahId": 6194,
        "endAyahId": 6197,
        "origin": "Mecca"
      },
      {
        "id": 107,
        "englishTransliteration": "The Small Kindness",
        "nameInEnglish": "Al Ma'un",
        "nameInArabic": "سورة المـاعون",
        "number": 107,
        "startAyahId": 6198,
        "endAyahId": 6204,
        "origin": "Mecca"
      },
      {
        "id": 108,
        "englishTransliteration": "The River in Paradise",
        "nameInEnglish": "Al Kawthar",
        "nameInArabic": "سورة الـكوثر",
        "number": 108,
        "startAyahId": 6205,
        "endAyahId": 6207,
        "origin": "Mecca"
      },
      {
        "id": 109,
        "englishTransliteration": "The Disbelievers",
        "nameInEnglish": "Al Kafirun",
        "nameInArabic": "سورة الـكافرون",
        "number": 109,
        "startAyahId": 6208,
        "endAyahId": 6213,
        "origin": "Mecca"
      },
      {
        "id": 110,
        "englishTransliteration": "The Help",
        "nameInEnglish": "An Nasr",
        "nameInArabic": "سورة الـنصر",
        "number": 110,
        "startAyahId": 6214,
        "endAyahId": 6216,
        "origin": "Medina"
      },
      {
        "id": 111,
        "englishTransliteration": "The Palm Fibre",
        "nameInEnglish": "Al Masad",
        "nameInArabic": "سورة الـمسد",
        "number": 111,
        "startAyahId": 6217,
        "endAyahId": 6221,
        "origin": "Mecca"
      },
      {
        "id": 112,
        "englishTransliteration": "The Purity",
        "nameInEnglish": "Al Ikhlas",
        "nameInArabic": "سورة الإخلاص",
        "number": 112,
        "startAyahId": 6222,
        "endAyahId": 6225,
        "origin": "Mecca"
      },
      {
        "id": 113,
        "englishTransliteration": "The Daybreak",
        "nameInEnglish": "Al Falaq",
        "nameInArabic": "سورة الـفلق",
        "number": 113,
        "startAyahId": 6226,
        "endAyahId": 6230,
        "origin": "Mecca"
      },
      {
        "id": 114,
        "englishTransliteration": "Mankind",
        "nameInEnglish": "An Nas",
        "nameInArabic": "سورة الـناس",
        "number": 114,
        "startAyahId": 6231,
        "endAyahId": 6236,
        "origin": "Mecca"
      }
]