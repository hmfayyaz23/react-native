import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { StyleSheet } from 'react-native';

export default class AppBar extends Component {
  render() {
    return (
      <View style={styles.header}>
        <Text style={styles.headerText}>Surahs</Text>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  header: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#2196F3',
  },
  headerText: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    padding: 18,
  },
});
