import React, { Component } from 'react';
import { FlatList } from 'react-native';
import { Surahs } from './SurahData';
//import components
import SurahCard from './SurahCard';

export default class MySurahs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //Assing a array to your surahs state
      surahs: Surahs,
    }
    console.log(this.state.surahs);
  }

  render() {
    //Destruct surahs from state.
    const { surahs } = this.state;
    return (
      <FlatList
        data={surahs}
        renderItem={({ item }) => <SurahCard surah={item} />}
        keyExtractor={item => item.nameInEnglish}
      />
    );
  }
}

