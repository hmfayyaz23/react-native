//import styleSheet for creating a css abstraction.
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    listItemContainer: {
        flex: 1,
        flexDirection: 'row',
        padding: 12,
        borderBottomWidth: 0.5,
        borderBottomColor:'#DCDCDC',
    },

    //surah number
    surahNumberContainer:{
        flex: 1,
        //backgroundColor: 'powderblue'
    },
    surahNumberImage: {
        height: 45,
        width: 45,
        alignItems: 'center',
        justifyContent: 'center',
    },
    surahNumber:{
        fontWeight:'bold',
        color:'#2196F3',
    },

    //translation 
    translationContainer:{
        flex: 3,
        //backgroundColor: 'skyblue',
    },
    nameInEnglish:{
        fontWeight:'bold',
        color:"#2196F3",
        fontSize:16
    },
    englishTransliteration:{
        fontWeight:'bold',
        color:"#616161",
        fontSize:12
    },

    //arabic 
    arabicContainer:{
        flex: 2.5,        
        justifyContent: 'center',
        alignItems:'flex-end'
    },
    nameInArabic:{    
        color:"#000000",
        fontSize:18
    },
})

export default styles;